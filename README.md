# Test Site

Test HTML5 Site to Clone


## To clone and use it in Apache2
```bash
cd /tmp
git clone https://gitlab.com/nuvens-public/test-site.git
cp -R test-site/site/* /var/www/html
```


## To clone and use it in NGinx
```bash
cd /tmp
git clone https://gitlab.com/nuvens-public/test-site.git
cp -R test-site/site/* /usr/share/nginx/html
```


The site will look like:
![Test Site](getit.png)
